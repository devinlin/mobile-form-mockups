/*
 * Copyright 2022 Devin Lin <devin@kde.org>
 * SPDX-License-Identifier: GPL-2.0-or-later
 */

import QtQuick 2.6
import QtQuick.Layouts 1.2
import QtQuick.Controls 2.2
import org.kde.kirigami 2.4 as Kirigami
import "mobileform"

// TODO:
// - make category visibility toggleable (animate in/out)
// - text input
// - widescreen card view
// - normal comboboxes for desktop?
// - number selector
// - port away from QQC2 control (it steals flicks from group header)
// - ensure press animation always plays

Kirigami.ScrollablePage {
    id: page
    title: "Mobile Form Layout"

    Kirigami.Theme.colorSet: Kirigami.Theme.Window
    Kirigami.Theme.inherit: false
    
    leftPadding: 0
    rightPadding: 0
    topPadding: Kirigami.Units.gridUnit
    bottomPadding: 0

    ColumnLayout {
        spacing: 0
        width: page.width
        
        FormCard {
            Layout.fillWidth: true
            
            contentItem: ColumnLayout {
                spacing: 0

                FormCardHeader {
                    title: "Buttons"
                }
                
                FormButtonDelegate {
                    id: delegate1
                    text: "Button"
                    onClicked: applicationWindow().pageStack.push("qrc:/contents/ui/gallery/DialogGallery.qml")
                }
                
                FormDelegateSeparator { above: delegate1; below: delegate2 }
                
                FormButtonDelegate {
                    id: delegate2
                    text: "Button 2"
                }
                
                FormDelegateSeparator { above: delegate2; below: delegate3 }
                
                FormButtonDelegate {
                    id: delegate3
                    text: "Notification Settings"
                    iconName: "notifications"
                }
            }
        }
        
        FormSectionText {
            text: "Use cards to denote relevant groups of settings."
        }
        
        // checkboxes
        FormCard {
            Layout.fillWidth: true
            Layout.topMargin: Kirigami.Units.largeSpacing
            
            contentItem: ColumnLayout {
                spacing: 0
                
                FormCardHeader {
                    title: "Checkboxes"
                }
                
                FormCheckDelegate {
                    id: checkbox1
                    text: "Check the first box"
                }
                
                FormCheckDelegate {
                    id: checkbox2
                    text: "Check the second box"
                }
                
                FormCheckDelegate {
                    id: checkbox3
                    text: "Check the third box"
                }
            }
        }
        
        // switches
        FormCard {
            Layout.fillWidth: true
            Layout.topMargin: Kirigami.Units.largeSpacing
            
            contentItem: ColumnLayout {
                spacing: 0
                
                FormCardHeader {
                    title: "Switches"
                }
                
                FormSwitchDelegate {
                    id: switch1
                    text: "Toggle the first switch"
                }
                
                FormDelegateSeparator { above: switch1; below: switch2 }
                
                FormSwitchDelegate {
                    id: switch2
                    text: "Toggle the second switch"
                }
                
                FormDelegateSeparator { above: switch2; below: switch3 }
                
                FormSwitchDelegate {
                    id: switch3
                    text: "Toggle the third switch"
                    description: "This is a description for the switch."
                }
            }
        }
        
        // dropdowns
        // large amount of options -> push a new page
        // small amount of options -> open dialog
        FormCard {
            Layout.fillWidth: true
            Layout.topMargin: Kirigami.Units.largeSpacing
            
            contentItem: ColumnLayout {
                spacing: 0
                
                FormCardHeader {
                    title: "Dropdowns"
                }
                
                FormComboBoxDelegate {
                    id: dropdown1
                    text: "Select a color"
                    currentValue: "Breeze Blue"
                }
                
                FormDelegateSeparator { above: dropdown1; below: dropdown2 }

                FormComboBoxDelegate {
                    id: dropdown2
                    text: "Select a shape"
                    currentValue: "Pentagon"
                }
                
                FormDelegateSeparator { above: dropdown2; below: dropdown3 }
                
                FormComboBoxDelegate {
                    id: dropdown3
                    text: "Select a time format"
                    currentValue: "Use System Default"
                }
            }
        }
        
        // radio buttons
        FormCard {
            Layout.fillWidth: true
            Layout.topMargin: Kirigami.Units.largeSpacing
            
            contentItem: ColumnLayout {
                spacing: 0
                
                FormCardHeader {
                    title: "Radio buttons"
                }
                
                FormRadioDelegate {
                    id: radio1
                    text: "Always on"
                }
                
                FormRadioDelegate {
                    id: radio2
                    text: "On during the day"
                }
                
                FormRadioDelegate {
                    id: radio3
                    text: "Always off"
                }
            }
        }

        // misc
        FormCard {
            Layout.fillWidth: true
            Layout.topMargin: Kirigami.Units.largeSpacing
            
            contentItem: ColumnLayout {
                spacing: 0
                
                AbstractFormDelegate {
                    id: slider1
                    Layout.fillWidth: true
                    
                    background: Item {}
                    
                    contentItem: RowLayout {
                        spacing: Kirigami.Units.gridUnit
                        Kirigami.Icon {
                            implicitWidth: Kirigami.Units.iconSizes.smallMedium
                            implicitHeight: Kirigami.Units.iconSizes.smallMedium
                            source: "brightness-low"
                        }
                        
                        Slider {
                            Layout.fillWidth: true
                        }
                        
                        Kirigami.Icon {
                            implicitWidth: Kirigami.Units.iconSizes.smallMedium
                            implicitHeight: Kirigami.Units.iconSizes.smallMedium
                            source: "brightness-high"
                        }
                    }
                }
                
                FormDelegateSeparator { above: slider1; below: textinput1 }
                
                AbstractFormDelegate {
                    id: textinput1
                    Layout.fillWidth: true
                    contentItem: RowLayout {
                        Label {
                            Layout.fillWidth: true
                            text: "Enter text"
                        }
                        
                        TextField {
                            Layout.preferredWidth: Kirigami.Units.gridUnit * 8
                            placeholderText: "Insert text…"
                        }
                    }
                }
                
                FormDelegateSeparator { above: textinput1; below: action1 }
                
                AbstractFormDelegate {
                    id: action1
                    Layout.fillWidth: true
                    contentItem: RowLayout {
                        Label {
                            Layout.fillWidth: true
                            text: "Do an action"
                        }
                        
                        Button {
                            text: "Do Action"
                            icon.name: "edit-clear-all"
                        }
                    }
                }
            }
        }
        
        // info block
        FormCard {
            Layout.fillWidth: true
            Layout.topMargin: Kirigami.Units.largeSpacing
            
            contentItem: ColumnLayout {
                spacing: 0
                
                FormCardHeader {
                    title: "Information"
                }

                FormTextDelegate {
                    id: info1
                    text: "Color"
                    description: "Blue"
                }
                
                FormDelegateSeparator { above: info1; below: info2 }
                
                FormTextDelegate {
                    id: info2
                    text: "Best Desktop Environment"
                    description: "KDE Plasma (Mobile)"
                }
                
                FormDelegateSeparator { above: info2; below: info3 }
                
                FormTextDelegate {
                    id: info3
                    text: "Best Dragon"
                    description: "Konqi"
                }
            }
        }
        
        FormSectionText {
            text: "Use the text form delegates to display information."
        }
        
        // TODO: "infinite" listview
    }
}
 
